import s10.Rectangle;
import s10.Triangle;

public class Task53C10 {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle("red",50,20);
        Rectangle rectangle2 = new Rectangle("black",25,25);
        System.out.println(rectangle1);
        System.out.println(rectangle2);
        System.out.println(rectangle1.getArea());
        System.out.println(rectangle2.getArea());
        Triangle triangel1 = new Triangle("pink",20, 5);
        Triangle triangel2 = new Triangle("yellow", 10, 25 );
        System.out.println(triangel1);
        System.out.println(triangel2);
        System.out.println(triangel1.getArea());
        System.out.println(triangel2.getArea());
    }
}
